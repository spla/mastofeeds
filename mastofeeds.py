import os
import feedparser
from mastodon import Mastodon
import psycopg2
import sys
import time

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    db_config_filepath = "db_config.txt"
    feeds_db =  get_parameter("feeds_db", db_config_filepath)
    feeds_db_user =  get_parameter("feeds_db_user", db_config_filepath)
    feeds_url =  get_parameter("feeds_url", db_config_filepath)

    # Load configuration from config file
    config_filepath = "config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath) # E.g., mastodon.social

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    #######################################################################

    publish = 0

    try:

        newsfeeds = feedparser.parse(feeds_url)
        print(newsfeeds.status)

    except:

        print(newsfeeds.status)
        sys.exit(0)

    for entry in newsfeeds.entries:

        title = entry['title']
        id = entry['id']
        link = entry['link']

        ###################################################################
        # check database if feed is already published

        try:

            conn = None
            conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute('select link from feeds where link=(%s)', (link,))

            row = cur.fetchone()
            if row == None:
                publish = 1
            else:
                publish = 0

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        ###########################################################

        if publish == 1:

            toot_text = str(title)+'\n'
            toot_text += str(link)

            print("Tooting...")
            print(toot_text)

            mastodon.status_post(toot_text, in_reply_to_id=None,)

            time.sleep(2)

            #########################################################

            insert_line = 'INSERT INTO feeds(id, link) VALUES (%s, %s)'

            conn = None

            try:

                conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                cur = conn.cursor()

                cur.execute(insert_line, (id, link,))

                conn.commit()

                cur.close()

            except (Exception, psycopg2.DatabaseError) as error:

                print(error)

            finally:

                if conn is not None:

                    conn.close()
        else:

            print("Any new feeds")
            sys.exit(0)
